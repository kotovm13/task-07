package com.epam.rd.java.basic.task7.db.dbUtils;

public abstract class DBManagerStatements {
    // Users statements
    public static final String FIND_ALL_USERS_STATEMENT = "SELECT * FROM users";
    public static final String INSERT_USER = "INSERT INTO users VALUES (DEFAULT, ?)";
    public static final String GET_USER_BY_LOGIN = "SELECT id, login FROM users WHERE login=?";
    public static final String DELETE_USERS = "DELETE FROM users WHERE login=?";
    public static final String SET_TEAM_FOR_USER = "INSERT INTO users_teams VALUES (?,?)";
    public static final String GET_ALL_TEAMS_FOR_USER = "SELECT users.login, teams.name FROM users JOIN users_teams ON users.id = users_teams.user_id AND users.login = ? JOIN teams ON users_teams.team_id = teams.id";

    // Teams statements
    public static final String FIND_ALL_TEAMS_STATEMENT = "SELECT * FROM teams";
    public static final String GET_TEAM_BY_NAME = "SELECT id, name FROM teams WHERE name=?";
    public static final String INSERT_TEAM = "INSERT INTO teams VALUES (DEFAULT, ?)";
    public static final String DELETE_TEAM = "DELETE FROM teams WHERE name=?";
    public static final String UPDATE_TEAM = "UPDATE teams SET name=? WHERE id=?";


    private DBManagerStatements() {

    }
}
