package com.epam.rd.java.basic.task7.db;

import java.sql.*;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;


import com.epam.rd.java.basic.task7.db.dbUtils.DBManagerStatements;
import com.epam.rd.java.basic.task7.db.entity.*;

public class DBManager {

	private static DBManager instance;
	private static final String CONNECTION_URL;
	private static final String CONNECTION_URL_PROPERTY_KEY = "connection.url";

	public static synchronized DBManager getInstance() {
		if (instance == null)
			instance = new DBManager();

		return instance;
	}

	static {
		FileReader reader = null;
		try {
			reader = new FileReader("app.properties");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		Properties properties = new Properties();
		try {
			properties.load(reader);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (reader != null) {
					reader.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		CONNECTION_URL = properties.getProperty(CONNECTION_URL_PROPERTY_KEY);
	}

	private DBManager() {

	}

	public List<User> findAllUsers() throws DBException {
		List<User> users = new ArrayList<>();
		Connection con = null;
		try {
			con = DriverManager.getConnection(CONNECTION_URL);

			Statement stmt = null;
				try {
					stmt = con.createStatement();

					ResultSet set = null;
					try {
						set = stmt.executeQuery(DBManagerStatements.FIND_ALL_USERS_STATEMENT);

						while (set.next()) {
							User user = new User();

							int id = set.getInt(1);
							String login = set.getString(2);

							user.setId(id);
							user.setLogin(login);

							users.add(user);
						}
					} catch (SQLException throwables) {
						throw new DBException("Exception at resultSet findAllUsers", throwables);
					} finally {
						close(set);
					}
				} catch (SQLException throwables) {
					throw new DBException("Exception at statement findAllUsers", throwables);
				} finally {
					close(stmt);
				}
		} catch (SQLException throwables) {
			throw new DBException("Bad connection at findAllUsers: " + throwables.getMessage(), throwables);
		} finally {
			close(con);
		}

		return users;
	}

	public boolean insertUser(User user) throws DBException {
		Connection con = null;

		try {
			con = DriverManager.getConnection(DBManager.CONNECTION_URL);
			PreparedStatement stm = null;
			try {
				stm = con.prepareStatement(DBManagerStatements.INSERT_USER, Statement.RETURN_GENERATED_KEYS);

				int k = 0;
				stm.setString(++k, user.getLogin());
				stm.executeUpdate();

				try (ResultSet set = stm.getGeneratedKeys()) {
					set.next();
					int id = set.getInt(1);
					user.setId(id);
				}

			} catch (SQLException throwables) {
				throw new DBException("Issues with resultSet at insertUser", throwables);
			} finally {
				close(stm);
			}
		} catch (SQLException throwables) {
			throw new DBException("Bad connection at insertUser", throwables);
		} finally {
			close(con);
		}

		return true;
	}

	public boolean deleteUsers(User... users) throws DBException {
		Connection con = null;
		try {
			con = DriverManager.getConnection(CONNECTION_URL);
			setAutoCommit(con, false);

			PreparedStatement stmt = null;
			try {
				stmt = con.prepareStatement(DBManagerStatements.DELETE_USERS);

				for (User user : users) {
					int k = 0;
					stmt.setString(++k, user.getLogin());
					stmt.addBatch();
				}

				stmt.executeBatch();
			} catch (SQLException throwables) {
				rollback(con);
				throw new DBException("Failed statement at deleteUsers", throwables);
			} finally {
				close(stmt);
			}
		} catch (SQLException throwables) {
			throw new DBException("Failed connection at deleteUsers", throwables);
		} finally {
			setAutoCommit(con, true);
			close(con);
		}

		return true;
	}

	public User getUser(String login) throws DBException {
		User user = new User();

		Connection con = null;
		try {
			con = DriverManager.getConnection(CONNECTION_URL);
			PreparedStatement stmt = null;

			try {
				stmt = con.prepareStatement(DBManagerStatements.GET_USER_BY_LOGIN);
				int k = 0;

				stmt.setString(++k, login);

				try (ResultSet set = stmt.executeQuery()) {
					set.next();
					int id = set.getInt("id");
					String userName = set.getString("login");

					user.setId(id);
					user.setLogin(userName);
				}
			} catch (SQLException throwables) {
				throw new DBException("Failed statement at getUser", throwables);
			} finally {
				close(stmt);
			}
		} catch (SQLException throwables) {
			throw new DBException("Failed connection at getUser", throwables);
		} finally {
			close(con);
		}

		return user;
	}

	public Team getTeam(String name) throws DBException {
		Team team = new Team();

		Connection con = null;
		try {
			con = DriverManager.getConnection(CONNECTION_URL);

			PreparedStatement stmt = null;
			try {
				stmt = con.prepareStatement(DBManagerStatements.GET_TEAM_BY_NAME);

				int k = 0;
				stmt.setString(++k, name);

				try (ResultSet set = stmt.executeQuery()) {
					set.next();

					int id = set.getInt("id");
					String teamName = set.getString("name");

					team.setId(id);
					team.setName(teamName);
				}
			} catch (SQLException throwables) {
				throw new DBException("Failed statement at getTeam", throwables);
			} finally {
				close(stmt);
			}
		} catch (SQLException throwables) {
			throw new DBException("Failed connection at getTeam", throwables);
		} finally {
			close(con);
		}

		return team;
	}

	public List<Team> findAllTeams() throws DBException {
		List<Team> teams = new ArrayList<>();

		try (Connection con = DriverManager.getConnection(CONNECTION_URL);
			 Statement stm = con.createStatement();
			 ResultSet set = stm.executeQuery(DBManagerStatements.FIND_ALL_TEAMS_STATEMENT)) {

			while (set.next()) {
				Team team = new Team();
				int id = set.getInt("id");
				String name = set.getString("name");

				team.setId(id);
				team.setName(name);

				teams.add(team);
			}

		} catch (SQLException throwables) {
			throw new DBException("Exception at findAllTeams", throwables);
		}

		return teams;
	}

	public boolean insertTeam(Team team) throws DBException {
		Connection con = null;
		try {
			con = DriverManager.getConnection(CONNECTION_URL);

			PreparedStatement stmt = null;
			try {
				stmt = con.prepareStatement(DBManagerStatements.INSERT_TEAM, Statement.RETURN_GENERATED_KEYS);

				int k = 0;
				stmt.setString(++k, team.getName());
				stmt.executeUpdate();

				try (ResultSet set = stmt.getGeneratedKeys()) {
					set.next();
					int id = set.getInt(1);
					System.out.println(id);
					team.setId(id);
				}

			} catch (SQLException throwables) {
				throw new DBException("Wrong statement at insertTeam", throwables);
			} finally {
				close(stmt);
			}
		} catch (SQLException throwables) {
			throw new DBException("Failed connection at insertTeam", throwables);
		} finally {
			close(con);
		}

		return true;
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		User u = getUser(user.getLogin());
		Team t;
		Connection con = null;
		try {
			con = DriverManager.getConnection(CONNECTION_URL);
			setAutoCommit(con, false);

			PreparedStatement stmt = null;
			try {
				stmt = con.prepareStatement(DBManagerStatements.SET_TEAM_FOR_USER);

				for (Team team : teams) {
					t = getTeam(team.getName());
					stmt.setInt(1, u.getId());
					stmt.setInt(2, t.getId());

					stmt.addBatch();
				}

				stmt.executeBatch();
			}catch (SQLException throwables) {
				rollback(con);
				throw new DBException("Wrong statement at setTeamsForUser", throwables);
			} finally {
				close(stmt);
			}
		} catch (SQLException throwables) {
			throw new DBException("Failed connection at setTeamsForUser", throwables);
		} finally {
			setAutoCommit(con, true);
			close(con);
		}

		return true;
	}

	public List<Team> getUserTeams(User user) throws DBException {
		List<Team> teams = new ArrayList<>();
		Connection con = null;
		try {
			con = DriverManager.getConnection(CONNECTION_URL);

			PreparedStatement stmt = null;
			try {
				stmt = con.prepareStatement(DBManagerStatements.GET_ALL_TEAMS_FOR_USER);
				stmt.setString(1, user.getLogin());

				try (ResultSet set = stmt.executeQuery()) {
					while (set.next()) {
						teams.add(Team.createTeam(set.getString(2)));
					}
				}
			} catch (SQLException throwables) {
				throw new DBException("Wrong statement at getUserTeams", throwables);
			} finally {
				close(stmt);
			}
		} catch (SQLException throwables) {
			throw new DBException("Failed connection at getUserTeams", throwables);
		} finally {
			close(con);
		}

		return teams;
	}

	public boolean deleteTeam(Team team) throws DBException {
		Connection con = null;
		try {
			con = DriverManager.getConnection(CONNECTION_URL);

			PreparedStatement stmt = null;
			try {
				int k = 0;
				stmt = con.prepareStatement(DBManagerStatements.DELETE_TEAM);
				stmt.setString(++k, team.getName());

				stmt.executeUpdate();

			} catch (SQLException throwables) {
				throw new DBException("Wrong statement at deleteTeam", throwables);
			} finally {
				close(stmt);
			}
		} catch (SQLException throwables) {
			throw new DBException("Failed connection at deleteTeam", throwables);
		} finally {
			close(con);
		}
		return true;
	}

	public boolean updateTeam(Team team) throws DBException {
		Connection con = null;
		try {
			con = DriverManager.getConnection(CONNECTION_URL);

			PreparedStatement stm = null;
			try {
				stm = con.prepareStatement(DBManagerStatements.UPDATE_TEAM);

				stm.setString(1,team.getName());
				stm.setInt(2, team.getId());

				stm.executeUpdate();
			} catch (SQLException throwables) {
				throw new DBException("Wrong statement at updateTeam", throwables);
			} finally {
				close(stm);
			}

		} catch (SQLException throwables) {
			throw new DBException("Failed connection at updateTeam", throwables);
		} finally {
			close(con);
		}
		return true;
	}

	private void close(AutoCloseable a) {
		if (a != null) {
			try {
				a.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private void rollback(Connection connection) {
		if (connection != null) {
			try {
				connection.rollback();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	private void setAutoCommit(Connection con, boolean state) {
		if (con != null) {
			try {
				con.setAutoCommit(state);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}
